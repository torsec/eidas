# eIDAS

This is a mirror for the official eIDAS source code available from 
[CEF DIGITAL](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS-Node).

Each eIDAS version has a specific branch that includes all the workarounds 
made by Politecnico di Torino.
